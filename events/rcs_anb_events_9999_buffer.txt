namespace = rcs_anb_buffer #You can't have an event trigger itself directly, these buffer events are for an event to trigger that will then re-trigger that same event, so that changing settings will not send you back to the previous settings event
country_event = { #Buffer event 6 - Development settings
	id = rcs_anb_buffer.1
	title = "rcs_anb_events.1.t"
	desc = "rcs_anb_events.1.d"
	picture = NOBLE_ESTATE_DEMANDS_eventPicture
	
	is_triggered_only = yes
	hidden = yes

	option = {
		country_event = { id = rcs_anb_events.1 }
	}
}