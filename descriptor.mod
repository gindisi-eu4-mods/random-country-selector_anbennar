name="Random Country Selector for Anbennar"
version="1.0.5"
picture="thumbnail.png"
dependencies={
	"Anbennar: A Fantasy Total Conversion"
	"Anbennar-PublicFork"
	"Anbennar"
	"Random Country Selector"
}
tags={
	"Gameplay"
	"Utilities"
	"Events"
	"Map"
}
supported_version="v1.37.*.*"
remote_file_id="3095035904"